from controller import app
from flask_jwt_extended import jwt_required
from flask import request
from model.path.user_model import get_users_model


# return list of all the registered users
@app.get("/users")
@jwt_required()
def get_users():
    return get_users_model()

from flask import request, Response

import logger
from controller import app
from model.decorators import account_admin
from model.path.games_model import games_model, games_filter_model, game_id, post_game_model, delete_game_id_model, \
    put_game_model

from flask_jwt_extended import jwt_required


# pagination with [cursor, cursor + limit]
# filter args are:
# - players, difficulty, duration, variation, name
@app.get("/games")
@jwt_required()
def get_games() -> list:
    args = request.args
    cursor = args.get('cursor', type=int)
    limit = args.get('limit', type=int)

    players = args.get('players', type=int, default=None)
    difficulty = args.get('difficulty', type=int, default=None)
    duration = args.get('duration', type=int, default=None)
    variation = args.get('variation', type=float, default=None)
    name = args.get('name', type=str, default=None)

    category = args.get('category', type=str, default=None)

    if name == '':
        name = None
    if players is None and difficulty is None and duration is None \
            and variation is None and name is None and category is None:
        res = games_model(cursor, limit)
        return res

    res = games_filter_model(cursor, limit, players, difficulty, duration, variation, name, category)
    return res


# get only one game by id
@app.get("/game/<int:id_game>")
@jwt_required()
def get_game_id(id_game: int) -> list:
    res = game_id(id_game)
    return res


# delete game by id
@app.delete("/game/<int:id_game>")
@jwt_required()
@account_admin()
def delete_game_id(id_game: int) -> Response:
    res = delete_game_id_model(id_game)
    logger.LOGGER.info('delete_game_id - 200')
    return res


# post a game with value in
# request.form as data & file
@app.post("/game")
@jwt_required()
@account_admin()
def post_game():
    res = post_game_model()
    logger.LOGGER.info('post_game - 200')
    return res


# post a game with value in
# request.form as data & file
@app.put("/game")
@jwt_required()
@account_admin()
def put_game():
    res = put_game_model()
    logger.LOGGER.info('put_game - 200')
    return res

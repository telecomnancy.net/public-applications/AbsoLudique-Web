from flask_jwt_extended import jwt_required

import logger
from controller import app
from model.path.auth_model import login_model, login_callback_model, logout_model, is_admin_model


@app.route("/login")
def login():
    res = login_model()
    logger.LOGGER.info('login - 200')
    return res


@app.route("/login/callback")
def login_callback():
    res = login_callback_model()
    logger.LOGGER.info('login_callback - 200')
    return res


@app.route("/logout")
@jwt_required()
def logout():
    res = logout_model()
    logger.LOGGER.info('logout - 200')
    return res


@app.route("/verify/token")
@jwt_required()
def verify():
    return "true"


@app.route("/user/admin")
@jwt_required()
def is_admin():
    return is_admin_model()

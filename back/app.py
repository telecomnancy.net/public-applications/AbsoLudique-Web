import logging
import os
from datetime import timedelta
from pathlib import Path

from dotenv import load_dotenv
from flask import Flask, request
from flask_cors import CORS
from flask_jwt_extended import JWTManager
from oauthlib.oauth2 import WebApplicationClient

import controller
from logger import logger_config
from setup_sql import db

# Entrypoint of the program

# To find the root of the project everywhere
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

# load all env variables
load_dotenv()
DURATION_TOKEN = os.getenv('DURATION_TOKEN')
LAUNCH_MOD = os.getenv('LAUNCH_MOD')
SIZE_LIMIT_MO_UPLOAD = int(os.getenv('SIZE_UPLOAD_LIMIT_FILE'))
UPLOAD_FOLDER = os.getenv('UPLOAD_FOLDER')
STATIC = os.getenv('STATIC_FOLDER')
ORIGIN_DEV = os.getenv('ORIGINS_DEV')
ORIGIN_OAUTH = os.getenv('ORIGINS_OAUTH')
FRONT_URI = os.getenv('FRONT_URI')
DB_HOST = os.getenv('DB_HOST')
DB_PORT = os.getenv('DB_PORT')
DB_NAME = os.getenv('DB_NAME')
DB_NAME_TEST = os.getenv('DB_NAME_TEST')

cors_header = ["Content-Type", "Authorization", "Access-Control-Allow-Credentials", "Access-Control-Allow-Origin"]

assert SIZE_LIMIT_MO_UPLOAD is not None
assert UPLOAD_FOLDER is not None
assert STATIC is not None
assert ORIGIN_DEV is not None
assert ORIGIN_OAUTH is not None
assert LAUNCH_MOD is not None
assert DURATION_TOKEN is not None
assert FRONT_URI is not None
assert DB_HOST is not None
assert DB_PORT is not None
assert DB_NAME is not None
assert DB_NAME_TEST is not None

#  load all secrets

load_dotenv(Path('.secret'))
GOOGLE_CLIENT_ID = os.getenv('GOOGLE_CLIENT_ID')
GOOGLE_CLIENT_SECRET = os.getenv('GOOGLE_CLIENT_SECRET')
GOOGLE_DISCOVERY_URL = os.getenv('GOOGLE_DISCOVERY_URL')
SECRET_KEY = os.getenv('SECRET_KEY')
DB_USER = os.getenv('DB_USER')
DB_PASS = os.getenv('DB_PASS')

assert DB_USER is not None
assert DB_PASS is not None
assert GOOGLE_CLIENT_ID is not None
assert GOOGLE_CLIENT_SECRET is not None
assert GOOGLE_DISCOVERY_URL is not None
assert SECRET_KEY is not None

# OAuth 2 client setup
client = WebApplicationClient(GOOGLE_CLIENT_ID)


def create_app(test=False):
    load_dotenv()

    # config the app to make app.py the start point but the actual program is one directory lower
    app_intern = Flask(__name__,
                       static_folder=STATIC)
    app_intern.config['SQLALCHEMY_DATABASE_URI'] = f"mariadb+mariadbconnector://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
    app_intern.config['SQLALCHEMY_BINDS'] = {
        'test_db': f"mariadb+mariadbconnector://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME_TEST}"
    }

    app_intern.config['JWT_SECRET_KEY'] = SECRET_KEY
    app_intern.config['CORS_HEADERS'] = cors_header
    app_intern.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
    app_intern.config["JWT_ACCESS_TOKEN_EXPIRES"] = timedelta(hours=float(DURATION_TOKEN))
    app_intern.config['MAX_CONTENT_LENGTH'] = SIZE_LIMIT_MO_UPLOAD * 1024 * 1024
    app_intern.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    ## add the logger
    logger_config()

    log = logging.getLogger('werkzeug')
    log.setLevel(logging.ERROR)


    ## add the controllers
    app_intern.register_blueprint(controller.app)

    CORS(app_intern, origins=["http://" + ORIGIN_DEV, "https://" + ORIGIN_DEV, ORIGIN_OAUTH, FRONT_URI])

    jwt = JWTManager(app_intern)

    #upload folder
    Path(UPLOAD_FOLDER).mkdir(parents=True, exist_ok=True)

    # database init
    db.init_app(app_intern)
    with app_intern.app_context():
        db.engine.pool._pre_ping = True
        db.create_all()

    @app_intern.errorhandler(413)
    def request_entity_too_large(error):
        return 'File Too Large', 413

    app_intern.logger.info("Start of the server with: "
                           "'\n- CORS_HEADERS = '" + str(cors_header) +
                           "'\n- UPLOAD_FOLDER = '" + UPLOAD_FOLDER +
                           "'\n- MAX_CONTENT_LENGTH = '" + str(SIZE_LIMIT_MO_UPLOAD) + "mo" +
                           "'\n- CORS = '" + ORIGIN_DEV + ", " + ORIGIN_OAUTH + "'"
                           )
    return app_intern


if __name__ == '__main__':
    load_dotenv()

    HOST = os.getenv('HOST')
    PORT = os.getenv('PORT')

    app = create_app()
    if LAUNCH_MOD == 'http':
        print("http mod")
        app.run(port=PORT, host=HOST)
    elif LAUNCH_MOD == 'https':
        print("https mod")
        app.run(port=PORT, host=HOST, ssl_context=('cert/abso.pem', 'cert/abso.key'))

from model.database.entity.user import User
from model.database.schema.user_schema import UserSchema


def get_users_model() -> list:
    users = User.query.all()
    res = []
    user_schema = UserSchema()
    for user in users:
        res.append(user_schema.dump(user))
    return res

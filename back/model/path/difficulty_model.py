
from flask import Response

import logger
from model.database.entity.difficultyEntity import Difficulty
from model.database.entity.boardgame import Boardgame
from setup_sql import db


# Post a difficulty in database
def post_difficulty_model(name: str) -> Response or str:
    difficulty = Difficulty.query.filter(Difficulty.name == name).first()
    if difficulty is not None:
        return Response("difficulty already exist", status=409)

    difficulty = Difficulty(name=name)
    db.session.add(difficulty)
    db.session.commit()
    logger.LOGGER.info('save of the difficulty done - 200 ')

    return str(difficulty.idDifficulty)


# delete a difficulty with the id
# or else return response 404
def delete_game_id_model(id_difficulty: int) -> Response:
    difficulty = Difficulty.query.filter(Difficulty.idDifficulty == id_difficulty).first()
    if difficulty is None:
        logger.LOGGER.warning('Difficulty don\'t exist id:%i - 404', id_difficulty)
        return Response("Difficulty with id:" + str(id_difficulty) + " doesn't exist", status=404)

    id_difficulty = difficulty.idDifficulty
    db.session.delete(difficulty)
    db.session.commit()
    Boardgame.query.filter(Boardgame.difficulty == id_difficulty).update({Boardgame.difficulty: None})

    return Response("Difficulty with id:" + str(id_difficulty) + " deleted", status=200)


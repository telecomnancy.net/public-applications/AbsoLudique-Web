import json

from model.database.enum.CategoryValueDic import category_value_json
from model.database.enum.DifficultyDic import difficulty_value_json, Difficulty


# return in json a dict to make sur the value are the same in front and back
def get_category_dict_model():
    return json.dumps(category_value_json())


def get_difficulty_dict_model():
    return json.dumps(difficulty_value_json())

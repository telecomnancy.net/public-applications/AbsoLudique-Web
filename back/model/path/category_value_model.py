
from flask import Response

import logger
from model.database.entity.boardgame import Boardgame
from model.database.entity.category import Category
from model.database.entity.categoryValue import CategoryValue
from model.database.entity.difficultyEntity import Difficulty
from setup_sql import db


# Post a category_value in database
def post_category_value_model(name: str) -> Response or str:
    category_value = CategoryValue.query.filter(Category.name == name).first()
    if category_value is not None:
        return Response("category_value already exist", status=409)

    category_value = Category(name=name)
    db.session.add(category_value)
    db.session.commit()
    logger.LOGGER.info('save of the category_value done - 200 ')

    return str(category_value.idCategory)


# delete a category_value with the id
# or else return response 404
def delete_category_value_id_model(id_category_value: int) -> Response:
    category_value = CategoryValue.query.filter(CategoryValue.idCategoryValue == id_category_value).first()
    if category_value is None:
        logger.LOGGER.warning('CategoryValue don\'t exist id:%i - 404', id_category_value)
        return Response("CategoryValue with id:" + str(d=id_category_value) + " doesn't exist", status=404)

    id_category_value = category_value.idCategory
    db.session.delete(category_value)
    db.session.commit()

    return Response("CategoryValue with id:" + str(id_category_value) + " deleted", status=200)


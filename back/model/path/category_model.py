from flask import Response

import logger
from model.database.entity.boardgame import Boardgame
from model.database.entity.category import Category
from model.database.enum.CategoryValueDic import exists_category_value
from setup_sql import db


# Post a category in database with the idBoardgame a id_game and value of the category as category_value
def post_category_model(id_game: int, category_value: int):
    boardgame = Boardgame.query.filter(Boardgame.idBoardgame == id_game).first()
    if boardgame is None:
        logger.LOGGER.warning('Boardgame don\' exist id:%i - 404', id_game)
        return Response("Boardgame with id:" + str(id_game) + " doesn't exist", status=404)

    if exists_category_value(category_value) is False:
        logger.LOGGER.warning('category ' + str(category_value) + ' is not a valid category - 406')
        return Response('category ' + str(category_value) + ' is not a valid category', status=406)

    category = Category(idBoardGame=id_game, category=category_value)
    db.session.add(category)
    db.session.commit()
    logger.LOGGER.info('save of the game done - 200 ')

    return str(category.idCategory)


# Put a category in database with the idBoardgame a id_game and value of the category as category_value
def put_category_model(id_game: int, category_value: int, remove: int):
    boardgame = Boardgame.query.filter(Boardgame.idBoardgame == id_game).first()
    if boardgame is None:
        logger.LOGGER.warning('Boardgame don\' exist id:%i - 404', id_game)
        return Response("Boardgame with id:" + str(id_game) + " doesn't exist", status=404)

    if exists_category_value(category_value) is False:
        logger.LOGGER.warning('category ' + str(category_value) + ' is not a valid category - 406')
        return Response('category ' + str(category_value) + ' is not a valid category', status=406)

    category = Category.query.filter(Category.idBoardGame == id_game, Category.category == int(category_value)).first()

    # add the category if it doesn't exist else does nothing
    if remove == 0:
        if category is None:
            category = Category(idBoardGame=id_game, category=int(category_value))
            db.session.add(category)
            db.session.commit()
            logger.LOGGER.info('save of the game done - 200 ')
            return str(category.idCategory)

    # remove the category if it exists
    else:
        if category is not None:
            db.session.delete(category)
            db.session.commit()
            logger.LOGGER.info('delete of the category done - 200 ')
            return 'delete of the category done'

    return str("nothing done")

from sqlalchemy import Enum

from model.database.enum.DifficultyDic import Difficulty
from setup_sql import db
SIZE_DESCRIPTION = 500


class Boardgame(db.Model):
    __tablename__ = "board_game"

    idBoardgame = db.Column(db.Integer, primary_key=True, autoincrement=True)

    name = db.Column(db.String(100), nullable=False)
    state = db.Column(db.String(25), nullable=False)
    description = db.Column(db.String(SIZE_DESCRIPTION), nullable=False)
    difficulty = db.Column(db.Integer, db.ForeignKey("difficulty.idDifficulty"))
    picture = db.Column(db.String(30), nullable=True)

    minPlayers = db.Column(db.Integer, nullable=False)
    maxPlayers = db.Column(db.Integer, nullable=False)
    duration = db.Column(db.Integer, nullable=False)  # in minutes

    category = db.relationship("Category", back_populates="boardGame")

    def __json__(self):
        return ['idBoardgame', 'name', 'difficulty', 'picture', 'minPlayers', 'maxPlayers', 'duration', 'category']


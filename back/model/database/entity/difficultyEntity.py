from setup_sql import db


class Difficulty(db.Model):
    __tablename__ = "difficulty"

    idDifficulty = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100), nullable=False)

from setup_sql import db


class CategoryValue(db.Model):
    __tablename__ = "category_value"

    idCategoryValue = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100), nullable=False)

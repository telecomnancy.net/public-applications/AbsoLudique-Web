from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field

from model.database.entity.category import Category


class CategorySchema(SQLAlchemySchema):
    class Meta:
        model = Category
        include_relationships = True
        load_instance = True

    idCategory = auto_field()

    idBoardGame = auto_field()
    category = auto_field()


from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field

from model.database.entity.user import User


class UserSchema(SQLAlchemySchema):
    class Meta:
        model = User
        include_relationships = False
        load_instance = True

    idUser = auto_field()
    firstname = auto_field()
    lastname = auto_field()
    email = auto_field()
    admin = auto_field()

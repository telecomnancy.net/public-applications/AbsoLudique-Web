import enum

import logger
from model.database.entity.categoryValue import CategoryValue


def get_category_value(id_category_value: int):
    category_value = CategoryValue.query.filter(CategoryValue.idCategoryValue == id_category_value).first()
    if category_value is None:
        logger.LOGGER.warning('difficulty don\' exist id:%i - 404', id_category_value)
        return None

    return category_value.name


def exists_category_value(id_category_value: int):
    category_value = CategoryValue.query.filter(CategoryValue.idCategoryValue == id_category_value).first()
    return category_value is not None


def category_value_json():
    category_values = CategoryValue.query.all()
    res = []
    for category_value in category_values:
        res.append({category_value.name: category_value.idCategoryValue})
    return res



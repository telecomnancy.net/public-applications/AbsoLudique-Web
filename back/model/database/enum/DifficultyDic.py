import logger
from model.database.entity.difficultyEntity import Difficulty


def get_difficulty(id_difficulty: int):
    difficulty = Difficulty.query.filter(Difficulty.idDifficulty == id_difficulty).first()
    if difficulty is None:
        logger.LOGGER.warning('difficulty don\' exist id:%i - 404', id_difficulty)
        return None

    return difficulty.name


def exists_difficulty(id_difficulty: int):
    difficulty = Difficulty.query.filter(Difficulty.idDifficulty == id_difficulty).first()
    return difficulty is not None


def difficulty_value_json():
    difficultys = Difficulty.query.all()
    res = []
    for diff in difficultys:
        res.append({diff.name: diff.idDifficulty})
    return res
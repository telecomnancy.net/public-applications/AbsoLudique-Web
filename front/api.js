// theses  3 const a value that can needed to be changed

export const BACK_PATH = process.env.NODE_ENV === "production" ? "https://api.abso.telecomnancy.net/" : "https://localhost:5000/";

export const REDIRECT_GOOGLE = "https://www.google.com/"

// function to simplify the fetch
export const fetcher = (path) => fetch(BACK_PATH + path, {
    mode: 'cors',
    credentials: "omit",
    redirect: 'follow',
    headers: {
        'Access-Control-Allow-Origin': [BACK_PATH, REDIRECT_GOOGLE]
    }
}).then(res => res.json())

export const fetcher_auth = (path, tokenValue) => fetch(BACK_PATH + path, {
    mode: 'cors',
    credentials: "omit",
    redirect: 'follow',
    headers: {
        'Access-Control-Allow-Origin': [BACK_PATH, REDIRECT_GOOGLE],
        'Authorization': 'Bearer ' + tokenValue,
    }
}).then(res => res.json())

// function to simplify the fetch
export const fetcher_post = (path, data) => fetch(BACK_PATH + path, {
    method: 'post',
    redirect: 'follow',
    credentials: "omit",
    body: data
})
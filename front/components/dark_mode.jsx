import {useThemeContext} from "../context";
import {useEffect, useState} from "react";

export default function dark_mode_toogle() {
    const {theme: theme, toggleTheme: toggleTheme} = useThemeContext();
    const [mounted, setMounted] = useState(false);

    useEffect(() => setMounted(true), []);

    if (!mounted) return <> </>;

    return (
        <div
            className="button-style pickable bg-grey-heavy text-s lg:inline-flex lg:w-auto w-full px-3 py-2 rounded text-white font-bold items-center flex z-10"
            onClick={toggleTheme}>
            <img src="/sun.png" alt="activer mode clair" width={20} height={20} className={
                theme !== 'light' ? '': 'hidden'}/>
            <img src="/moon.png" alt="activer mode sombre" width={20} height={20} className={
                theme !== 'dark' ? '': 'hidden'}/>

        </div>
    );
};
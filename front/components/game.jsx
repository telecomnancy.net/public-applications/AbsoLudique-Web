import {BACK_PATH} from "../api";
import Image from "next/image";
import value_to_difficulty, {setupValueToDifficulty} from "../convert/value_to_difficulty";
import {useEffect, useState} from "react";
import {setup_valueToCategory} from "../convert/value_to_category";
import token from "../pages/api/token";

// Game display of one game with minimal information
export default function Game({game}) {

    return (
        <>
            <div className='w-fit h-fit rounded-0p75 margin-10 shadow-el padding-1'>
                <h1 className='text-1p6 center-text'>{game.name}</h1>
                <img
                    src={`${BACK_PATH + "static/" + ((game.picture !== '') ? 'upload/' + game.picture : 'default.png')}`}
                    alt={`${'image du jeu ' + game.name}`} className='img-game img-center padding-1 z-first'/>

                <div className='flex flex-game-showcase' title="Nombre de joueurs">

                    <div className='bg-grey-lite rounded-0p25 inline-flex margin-left-10 margin-top-5 padding-0p4'>
                        <Image title="Nombre de joueurs" src="/players.png" alt="Nombre de joueurs" width="20"
                               height="20"/>
                        <p className='margin-left-5'>{game.minPlayers}-{game.maxPlayers}</p>
                    </div>

                    <div className='bg-grey-lite rounded-0p25 inline-flex margin-left-10 margin-top-5 padding-0p4'
                         title="Durée d'une partie">
                        <span className='img-wrapped'>
                            <Image title="Durée d'une partie" src="/duration.png" alt="Durée d'une partie" width="25"
                                   height="25"/>
                        </span>
                        <p className='margin-left-5'>{game.duration} minutes</p>
                    </div>
                </div>

            </div>
        </>
    )
}
import Game from "./game";
import useSWRInfinite from "swr/infinite";
import {useEffect, useState} from "react";
import ModalGame from "./modal_game";
import {BACK_PATH, fetcher_auth, REDIRECT_GOOGLE} from "../api";
import Image from "next/image";
import {setupValueToDifficulty} from "../convert/value_to_difficulty";

let PAGE_SIZE = 30

// showcase component of all the games with the game.jsx component for each one the modal_game for the detailed information
export default function cabinet(token) {
    const [player, setPlayer] = useState();
    const [difficulty, setDifficulty] = useState();
    const [duration, setDuration] = useState();
    const [key, setKey] = useState(-1);
    const [name, setName] = useState('');
    const [show, setShow] = useState(false);
    const [data_game, setDataGame] = useState(null);
    const [tokenValue, setTokenValue] = useState(token)

    function setFilterVisible(verticalMenu, showcase, toggleArrow) {
        verticalMenu.classList.remove('hidden')
        verticalMenu.classList.add('block')
        toggleArrow.classList.remove('hidden-toggle-arrow')
        toggleArrow.classList.add('visible-toggle-arrow')
        showcase.classList.add('decal-vertical-bar-showcase')
    }

    function setFilterHidden(verticalMenu, showcase, toggleArrow) {
        verticalMenu.classList.remove('block')
        verticalMenu.classList.add('hidden')
        toggleArrow.classList.remove('visible-toggle-arrow')
        toggleArrow.classList.add('hidden-toggle-arrow')
        showcase.classList.remove('decal-vertical-bar-showcase')
    }

    function toggleVisibilityFilter() {
        let verticalMenu = document.getElementById('vertical-menu')
        let showcase = document.getElementById('showcase')
        let toggleArrow = document.getElementById('toggle-arrow')

        // When the user clicks on the button, toggle between hiding and showing the dropdown content
        if (verticalMenu.classList.contains('hidden')) {
            setFilterVisible(verticalMenu, showcase, toggleArrow)
        } else {
            setFilterHidden(verticalMenu, showcase, toggleArrow)
        }

    }

    const [valueToDifficulty, setvalueToDifficulty] = useState(new Map());
    const [category_values, setCategory_values] = useState([]);
    const [category_key, setCategory_key] = useState([]);
    const initialCategories = category_values.map(() => false);
    const [categories, setCategories] = useState(initialCategories);


    useEffect(() => {
        fetch('/api/token')
            .then(response => {
                response.json()
                    .then(value => {
                        setTokenValue(value)
                        let valueToCategory = new Map()
                        fetcher_auth('dict/category', value)
                            .then((array) => {
                                array.forEach((value) => {
                                    Object.keys(value).forEach((key) => {
                                        valueToCategory.set(value[key], key);
                                    });
                                });

                                let valueToDiffucultytmp = new Map()
                                setupValueToDifficulty(valueToDiffucultytmp, value)
                                setvalueToDifficulty(valueToDiffucultytmp)

                                setCategory_values(Array.from(valueToCategory.values()));
                                setCategory_key(Array.from(valueToCategory.keys()));

                                let category_valuestmp = Array.from(valueToCategory.entries());
                                setCategory_values(category_valuestmp);
                                mutate()
                            })
                    })
            });
        let showcase = document.getElementById('showcase')
        const mediaQuery = window.matchMedia('(max-width: 450px)');
        if (mediaQuery.matches) {
            let verticalMenu = document.getElementById('vertical-menu')
            let toggleArrow = document.getElementById('toggle-arrow')

            setFilterHidden(verticalMenu, showcase, toggleArrow) // set the filter to hidden on mobile at the beggining
        }

        let startX;
        let startY;
        let distX;
        let distY;
        let threshold = 100; // Minimum distance required for a swipe
        let allowedTime = 300; // Maximum time allowed to complete the swipe
        let elapsedTime;
        let startTime;
        const handleTouchStart = (e) => {
            const touchObj = e.changedTouches[0];
            startX = touchObj.pageX;
            startY = touchObj.pageY;
            startTime = new Date().getTime(); // Record start time
        };

        const handleTouchEnd = (e) => {
            console.log('ended')
            const touchObj = e.changedTouches[0];
            distX = touchObj.pageX - startX;
            distY = touchObj.pageY - startY;
            elapsedTime = new Date().getTime() - startTime;

            // Check if it's a swipe
            if (elapsedTime <= allowedTime) {
                // Check if it's a horizontal swipe and meets the distance threshold
                if (Math.abs(distX) >= threshold && Math.abs(distY) <= threshold) {
                    // Horizontal swipe detected
                    if (distX > 0) {
                        // Right swipe
                        console.log('Right swipe detected');
                        // Call your function or perform your action here
                    } else {
                        // Left swipe
                        console.log('Left swipe detected');
                        // Call your function or perform your action here
                    }
                }
            }

        }

        let swipeable = document.getElementById('swippeable')
        swipeable.addEventListener('touchstart', handleTouchStart);
        swipeable.addEventListener('touchend', handleTouchEnd);

        // Clean up event listeners on component unmount
        return () => {
            swipeable.removeEventListener('touchstart', handleTouchStart);
            swipeable.removeEventListener('touchend', handleTouchEnd);
        };

    }, []);


    const {
        data,
        mutate,
        size,
        setSize,
        isValidating,
        isLoading
    } = useSWRInfinite(
        (index) => 'games?cursor=' + ((index * PAGE_SIZE) + 1) + '&limit=' + PAGE_SIZE
            + ((player !== '') ? '&players=' + player : '')
            + ((difficulty !== '') ? '&difficulty=' + difficulty : '')
            + ((duration !== '') ? '&duration=' + duration : '')
            + ((name !== '') ? '&name=' + name : '')
            + ('&category=' + categories
                .map((value, index) => [value, index])
                .filter((value, index) => value[0] === true)
                .map((value, index) => {
                    return category_key[value[1]]
                })
                .join('/'))
        ,
        (path) => {
            return fetch(BACK_PATH + path, {
                mode: 'cors',
                credentials: 'omit',
                redirect: 'follow',
                headers: {
                    'Authorization': 'Bearer ' + tokenValue,
                    'Access-Control-Allow-Origin': [BACK_PATH, REDIRECT_GOOGLE]
                }
            }).then(res => res.json())
        },
    );



    const games = data ? [].concat(...data) : [];
    const isLoadingMore =
        isLoading || (size > 0 && data && typeof data[size - 1] === "undefined");
    const isEmpty = data?.[0]?.length === 0;
    const isReachingEnd =
        isEmpty || (data && data[data.length - 1]?.length < PAGE_SIZE);

    return (
        <>
            <ModalGame game={data_game} key="keymodalgame" isShow={show} setShow={setShow} tokenValue={token}
                       deleteCallback={mutate}/>

            {/* filter div*/}
            <div className="vertical-bar bg-grey-litle-plain" id='vertical-menu'>
                <div className='padding-bar'>
                    <h1 className='text-1p6 center-text margin-top-bottom-5 title-color'>Filtre</h1>
                    <hr className=''></hr>

                    <div className='centered-container'>
                        <p className='margin-top-20 name-game-align'>Nom du jeu</p>
                        <input className='input-name input'
                               value={name}
                               onChange={(e) => setName(e.target.value)}
                        /><br></br>

                        <div>
                            <input className='input-player margin-top-20'
                                   value={player}
                                   onChange={(e) => setPlayer(e.target.value)}
                            /> <span>Joueurs</span> <br></br>
                        </div>
                        <div>
                            <input className='input-duration margin-top-20'
                                   value={duration}
                                   onChange={(e) => setDuration(e.target.value)}
                            /> <span>Minutes</span> <br></br>
                        </div>
                    </div>

                    <div className='difficulty-div'>
                        <p className='margin-top-20'>Difficulté</p>
                        <select
                            value={difficulty}
                            onChange={(e) => setDifficulty(e.target.value)}
                            className='input-difficulty'>

                            <option value='' label="aucun choix"></option>
                            {Array.from(valueToDifficulty).map((value) => {
                                return <option key={value[0]} value={value[0]} label={value[1]}></option>
                            })}

                        </select>
                    </div>

                    <hr className='margin-top-20'></hr>

                    <h1 className='margin-top-20 text-1p1 center-text title-color'>Catégories</h1>
                    <div className="flex flex-category flex-column margin-top-20 justify-center">

                        {category_values.map((value, index) => {
                            return (
                                <div key={value} className="mb-[0.125rem] block min-h-[1.5rem] pl-[1.5rem]">
                                    <input
                                        className="relative float-left mt-[0.15rem] mr-[6px] -ml-[1.5rem] h-[1.125rem] w-[1.125rem] appearance-none rounded-[0.25rem] border-[0.125rem] border-solid border-[rgba(0,0,0,0.25)] bg-white outline-none before:pointer-events-none before:absolute before:h-[0.875rem] before:w-[0.875rem] before:scale-0 before:rounded-full before:bg-transparent before:opacity-0 before:shadow-[0px_0px_0px_13px_transparent] before:content-[''] checked:border-primary checked:bg-primary checked:before:opacity-[0.16] checked:after:absolute checked:after:ml-[0.25rem] checked:after:-mt-px checked:after:block checked:after:h-[0.8125rem] checked:after:w-[0.375rem] checked:after:rotate-45 checked:after:border-[0.125rem] checked:after:border-t-0 checked:after:border-l-0 checked:after:border-solid checked:after:border-white checked:after:bg-transparent checked:after:content-[''] hover:cursor-pointer hover:before:opacity-[0.04] hover:before:shadow-[0px_0px_0px_13px_rgba(0,0,0,0.6)] focus:shadow-none focus:transition-[border-color_0.2s] focus:before:scale-100 focus:before:opacity-[0.12] focus:before:shadow-[0px_0px_0px_13px_rgba(0,0,0,0.6)] focus:before:transition-[box-shadow_0.2s,transform_0.2s] focus:after:absolute focus:after:z-[1] focus:after:block focus:after:h-[0.875rem] focus:after:w-[0.875rem] focus:after:rounded-[0.125rem] focus:after:bg-white focus:after:content-[''] checked:focus:border-primary checked:focus:bg-primary checked:focus:before:scale-100 checked:focus:before:shadow-[0px_0px_0px_13px_#3b71ca] checked:focus:before:transition-[box-shadow_0.2s,transform_0.2s] checked:focus:after:ml-[0.25rem] checked:focus:after:-mt-px checked:focus:after:h-[0.8125rem] checked:focus:after:w-[0.375rem] checked:focus:after:rotate-45 checked:focus:after:rounded-none checked:focus:after:border-[0.125rem] checked:focus:after:border-t-0 checked:focus:after:border-l-0 checked:focus:after:border-solid checked:focus:after:border-white checked:focus:after:bg-transparent"
                                        type="checkbox"
                                        value={categories[index]}
                                        checked={categories[index]}
                                        onChange={() => {
                                            const updatedCategories = [...categories];
                                            updatedCategories[index] = !updatedCategories[index];
                                            setCategories(updatedCategories);
                                        }}
                                        id={`{checkboxDefault${index}`}/>
                                    <label
                                        className="inline-block pl-[0.15rem] margin-left-5 hover:cursor-pointer"
                                        htmlFor={`{checkboxDefault${index}`}>
                                        {value[1]}
                                    </label>
                                </div>
                            )
                        })}

                    </div>

                    <button className='button-style button-smaller margin-top-20 reset-button' onClick={() => {
                        setDuration('')
                        setPlayer('')
                        setDifficulty('')
                        setName('')
                        const updatedCategories = [...categories];
                        for (let i = 0; i < categories.length; i++) {
                            updatedCategories[i] = false
                        }
                        setCategories(updatedCategories);
                        mutate()
                    }
                    }>
                        Réinitialiser
                    </button>

                </div>
            </div>

            {/* showcase div */}
            <div className='scrollable-vertical decal-vertical-bar-showcase border-left-grey-heavy' id='showcase'>
                <Image src='/toggle-arrow.png' alt='ouvrir les filtres' width='40' height='40'
                       className='control-show-filter visible-toggle-arrow' id='toggle-arrow'
                       onClick={toggleVisibilityFilter}/>
                <div className='flex flex-wrap padding-20 flex-games' id='swippeable'>
                    {games.length === 0 && !isReachingEnd &&
                        <div className="center-content-fixed">
                            <Image title="Chargement" src="/loading.gif" alt="Chargement" width="50" height="100"/>Chargement...
                        </div>
                    }
                    {games && games.map((game) => {
                        if (game.idBoardgame !== null && game.idBoardgame !== undefined)
                            return (
                                <div className='pickable'
                                     key={game.idBoardgame}
                                     onClick={() => {
                                         setDataGame(game)
                                         setKey(game.idBoardgame)
                                         setShow(true)
                                     }}>
                                    {/* adminValue &&
                                       <Link href={`/game/${game.idBoardgame}`} className='absolute radius-10 shadow-el background-white padding-2'>
                                            <Image  src="/edit.png" alt="Éditer le jeu" width="30" height="30"
                                                                className=''></Image>
                                        </Link>*/}
                                    <Game key={game.idBoardgame} game={game}/>
                                </div>
                            )
                        else
                            return <></>
                    })}
                </div>
               <div className='center-text padding-20'>
                    {games && !isReachingEnd && <>
                        <button className='button-style button-larger mb-2'
                                disabled={isLoadingMore || isReachingEnd}
                                onClick={() => setSize(size + 1)}
                        >
                            {isLoadingMore
                                ? ''
                                : "voir plus"}
                        </button>
                    </>
                    }
                    {isReachingEnd && games.length === 0 && !isLoading &&
                        <div className='text-2 center-content-fixed'>Aucun jeu trouvé</div>
                    }
                </div>
            </div>


        </>
    )
}
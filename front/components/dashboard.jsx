import {useEffect, useState} from "react";
import {fetcher_auth} from "../api";

export default function dashboard(token) {
    const [users, setUsers] = useState([]);
    const [usersSave, setUsersSave] = useState([]);
    const [tokenValue, setTokenValue] = useState(token)
    const [searchValue, setSearchValue] = useState("")

    useEffect(() => {
        fetch('/api/token')
            .then(response => {
                response.json()
                    .then(value => {
                        setTokenValue(value)
                        let users_tmp = []
                        fetcher_auth('/users', value).then((data) => {
                            data.forEach((user) => {
                                users_tmp.push(user)
                            })
                            setUsers(users_tmp)
                            setUsersSave(users_tmp)
                        });
                    })
            })
    }, [])

    function search_filter(user) {
        let searchValuelower = searchValue.toLowerCase()
        return (user.firstname.toLowerCase().includes(searchValuelower) ||
                user.lastname.toLowerCase().includes(searchValuelower))
    }

    return (
        <div>
            <h1>Membres</h1>
            <input value={searchValue} onChange={e => setSearchValue(e.target.value)}/>
            <div className="table-container">
                <table className="shadow-el table-user">
                    <thead>
                    <tr className="table-head-user">
                        <th>Prénom</th>
                        <th>Nom</th>
                        <th className="mail-col">Email</th>
                        <th className="last-col-user">Rôle</th>
                    </tr>
                    </thead>
                    <tbody className="padding-0p4 table-body-user">
                    {users.map((user) => (
                        <tr key={"table-user"+user.idUser} className={search_filter(user) ? '': 'hidden'}>
                            <td>{user.firstname}</td>
                            <td>{user.lastname}</td>
                            <td className="mail-col">{user.email}</td>
                            <td className="last-col-user">{user.admin ? 'admin' : 'utilisateur'}</td>
                        </tr>
                    ))}
                    </tbody>
                </table>
            </div>
        </div>
    )
}
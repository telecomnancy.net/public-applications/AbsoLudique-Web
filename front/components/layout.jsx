import Navbar from "./navbar";
import Head from "next/head";
import {useRef} from "react";

//Base page layout of the server
export default function Layout({children}) {
    const navbarComp = useRef(null);

    const triggerNavbar = () => {
        navbarComp.current.handleClickForced(false)
    }

    return (
        <>
            <Head>
                <title>Abso'Ludique</title>
                <link rel='icon' href='/favicon.ico'/>
            </Head>
            <Navbar key="navbar-unique" ref={navbarComp}/>
            <div className='height-100vh' onClick={triggerNavbar}>
                {children}
            </div>
        </>
    );
}
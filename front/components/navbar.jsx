import Link from 'next/link';
import React, {useContext, useState} from 'react';
import Image from 'next/image'
import {useRouter} from "next/router";
import {Context} from "../context";
import dark_mode_toogle from "./dark_mode";

// Navbar of the server
function Navbar(props, ref) {
    const {adminValue, setAdmin} = useContext(Context);
    // add here to put it in the navbar
    const navigationRoutes = [
        //{ name:"", path:"" }
    ];
    const buttonAction = [
        {component: dark_mode_toogle()}
    ]
    const navigationRoutesAdmin = [
        {name: "Ajout jeu", path: "ajout/jeu"},
        {name: "Tableau de Bord", path: "dashboard"},
    ];


    const [active, setActive] = useState(false);
    const router = useRouter();

    const handleClick = () => {
        setActive(!active);
    };
    const handleClickForced = (value) => {
        setActive(value);
    };

    React.useImperativeHandle(ref, () => ({
        handleClickForced: handleClickForced,
    }));


    async function logout() {
        await fetch('/api/logout', {method: 'POST'})
        handleClick()
        setAdmin(false)
        await router.push('/login')
    }

    return (
        <div key="navbar-component" className="navbar-height z-10 bg-grey-heavy">

            <nav className='flex padding-margin-0 test items-center flex-wrap bg-grey-heavy dark:bg-grey-heavy p-3 z-first'>

                {/* left part */}
                <Link href='/' className='inline-flex items-center p-2 mr-4 '>
                    <Image src="/logo.svg" alt="logo d'abso'ludique" width="80" height="60"/>

                    <span onClick={() => handleClickForced(false)}
                          className='text-1p6 text-white font-bold uppercase tracking-wide select title-abso'>
              Abso'Ludique
            </span>
                </Link>

                {/* hamburger nav */}
                {router.pathname !== '/login' &&

                    <button
                        className='inline-flex p-3 hover:bg-grey-medium rounded-0p25 lg:hidden text-white ml-auto mr-2 hover:text-white outline-none'
                        onClick={handleClick}
                    >
                        <svg
                            className='w-6 h-6'
                            fill='none'
                            stroke='currentColor'
                            viewBox='0 0 24 24'
                            xmlns='http://www.w3.org/2000/svg'
                        >
                            <path
                                strokeLinecap='round'
                                strokeLinejoin='round'
                                strokeWidth={2}
                                d='M4 6h16M4 12h16M4 18h16'
                            />
                        </svg>
                    </button>}

                <div
                    className={`${
                        active ? '' : 'hidden'
                    }` + " w-full lg:inline-flex lg:flex-grow lg:w-auto"}
                >

                    {/* hamburger content part */}
                    {router.pathname !== '/login' &&
                        <div
                            className='lg:inline-flex lg:flex-row lg:ml-auto lg:w-auto w-full lg:items-center bg-grey-heavy items-start bg-grey-heavy flex flex-col lg:h-auto z-10'>
                            {/* if is admin */}
                            {adminValue === true && navigationRoutesAdmin.map((route) => {
                                const isActive = router.asPath === '/' + route.path
                                return (
                                    <Link key={"route-name-admin"+route.name} onClick={() => handleClickForced(false)} href={`/${route.path}`}
                                          className={`${isActive ? 'active-path' : 'hover:bg-grey-medium hover:text-white'} bg-grey-heavy z-10 text-s lg:inline-flex lg:w-auto w-full px-3 py-2 rounded text-white font-bold items-center justify-center`}>
                                        <div className={`${isActive ? 'active-text' : 'not-active-text'}`}>
                                            {route.name}
                                        </div>
                                    </Link>)
                            })}
                            {navigationRoutes.map((route) => {
                                const isActive = router.asPath === '/' + route.path
                                return (
                                    <Link key={"route-name"+route.name} onClick={() => handleClickForced(false)} href={`/${route.path}`}
                                          className={`${isActive ? 'active-path' : 'hover:bg-grey-medium hover:text-white'} bg-grey-heavy z-10 text-s lg:inline-flex lg:w-auto w-full px-3 py-2 rounded text-white font-bold items-center justify-center`}>
                                        <div className={`${isActive ? 'active-text' : 'not-active-text'}`}>
                                            {route.name}
                                        </div>
                                    </Link>)
                            })}
                            <div onClick={logout}
                                 className="select text-s lg:inline-flex lg:w-auto w-full px-3 py-2 bg-grey-heavy hover:bg-grey-medium hover:text-white rounded z-10 text-white font-bold items-center justify-center">
                                Déconnexion
                            </div>
                            {buttonAction.map((button) => {
                                return button.component
                            })}
                        </div>
                    }

                </div>
            </nav>
        </div>
    );
};


export default React.forwardRef(Navbar);
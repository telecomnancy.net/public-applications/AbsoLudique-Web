const {createServer} = require("https");
const {parse} = require("url");
const next = require("next");
const fs = require("fs");
const dotenv = require("dotenv");
const {bool} = require("sharp/lib/is");

// Load environment variables from .env file
dotenv.config();

const port = process.env.PORT || 3000;
const dev = process.env.DEV === true || bool(false);
const hostname = process.env.HOSTNAME || "localhost";
const app = next({dev, hostname, port});
const handle = app.getRequestHandler();

const httpsOptions = {
    key: fs.readFileSync("./cert/abso.key"),
    cert: fs.readFileSync("./cert/abso.pem")
};

// create the next application
app.prepare().then(() => {
    createServer(httpsOptions, (req, res) => {
        const parsedUrl = parse(req.url, true);
        handle(req, res, parsedUrl);
    }).listen(port, (err) => {
        if (err) throw err;
        console.log(`> Ready on https://${hostname}:${port}`);
    });
});

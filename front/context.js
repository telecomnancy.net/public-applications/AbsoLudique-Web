import React, {createContext, useContext, useState} from "react"
import {useTheme} from "next-themes";

// Prove a context about how the differents components can communicate between them
// and the hierarchy
export const Context = createContext({
    token: '',
    setToken: () => {
    },
    adminValue: false,
    setAdmin: () => {
    },
    theme: 'dark',
    toggleTheme: () => {
    }
})

export const ContextProvider = ({children}) => {
    const [admin, setAdmin] = useState(false);
    const [token, setToken] = useState('');

    const { systemTheme, resolvedTheme, setTheme: switchTheme} = useTheme();
    const [theme, setTheme] = useState(systemTheme);

    const toggleTheme = () => {
        const newTheme = resolvedTheme === 'light' ? 'dark' : 'light';
        setTheme(newTheme);
        switchTheme(newTheme);
    };

    return (
        <Context.Provider value={{adminValue: admin, setAdmin: setAdmin, theme:theme, toggleTheme:toggleTheme, token:token, setToken: setToken}}>
            {children}
        </Context.Provider>
    )
}

export const useThemeContext = () => {
  return useContext(Context);
};



import '../styles/main.css';
import Layout from "../components/layout";
import React from "react";
import {ContextProvider} from "../context";
import {ThemeProvider} from "next-themes";

export default function MyApp({
                                  Component, pageProps: {session, ...pageProps}
                              }) {

    return (
         <ThemeProvider defaultTheme="dark" attribute="class">
            <ContextProvider>
                    <Layout>
                        <Component {...pageProps} />
                    </Layout>
            </ContextProvider>
         </ThemeProvider>
    );
}

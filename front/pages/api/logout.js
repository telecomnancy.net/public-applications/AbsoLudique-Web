import cookie from "cookie";
// This incredible complex function is just to set the cookie to "logged out" and delete it
export default async function logout(req, res) {
    const setCookie = cookie.serialize('jwt', 'logged out', {
        httpOnly: true,
        secure: process.env.NODE_ENV === 'production',
        maxAge: -1,
        sameSite: 'strict',
        path: '/',
    });
    res.setHeader('Set-Cookie', setCookie)
    res.end()
}

import {fetcher, fetcher_auth} from "../api";

const maptmp = new Map();

export function setup_valueToCategory(map, tokenValue) {
    if (maptmp.size > 0) {
        maptmp.forEach((value, key) => {
            map.set(key, value)
        })
        return
    }
    fetcher_auth('dict/category', tokenValue).then(array => {
        array.forEach(value => {
            Object.keys(value)
                .forEach(key => {
                    maptmp.set(value[key], key)
                    map.set(value[key], key)
                })
        })
    }).catch(error => {
        console.log(error)
    })
}

export default function value_to_category(map, value) {

    return map.get(value)
}